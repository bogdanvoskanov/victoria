import Catalog from './components/Catalog/Catalog';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Rules from './components/Rules/Rules';

function App() {
  return (
    <>
      <Header />
      <Rules />
      <Catalog />
      <Footer />
    </>
  );
}

export default App;
