import React from 'react';

import styles from './Rules.module.css';

import rule1 from '../../assets/images/rule-1.svg';
import rule2 from '../../assets/images/rule-2.svg';
import rule3 from '../../assets/images/rule-3.svg';
import rule4 from '../../assets/images/rule-4.svg';

import leaflet from '../../assets/leaflet.pdf';

import thomasLogo from '../../assets/images/thomas-grey.png';

import littleCard from '../../assets/images/little-card.png';

const WIDTH = window.innerWidth;

const Rules = () => {
  const [rules, setRules] = React.useState([
    {
      image: rule1,
      description:
        'Количество выдаваемых наклеек зависит от итоговой суммы покупки без учета всех предоставленных скидок.'
    },
    {
      image: rule2,
      description: 'Собирайте наклейки и вклеивайте их в буклет.'
    },
    {
      image: rule3,
      description:
        'Набрав необходимое количество наклеек, вклейте их в поле буклета. Выберите товар, находящийся в свободном доступе на стойках с логотипом',
      hasImage: true
    },
    {
      image: rule4,
      description:
        'Предъявите на кассе выбранный товар и буклет с вклеенными в него наклейками.'
    }
  ]);
  return (
    <div className={styles.rules}>
      <h2 className={styles.title}>Правила акции</h2>
      <div className='container'>
        <div className={styles.wrap}>
          {rules.map((rule, index) => {
            return (
              <div key={index} className={styles.rule}>
                <img src={rule.image} alt='Rule' />
                <p className={styles.description}>
                  {rule.description}{' '}
                  <span className={styles.thomasGrey}>
                    {rule.hasImage ? (
                      <img src={thomasLogo} alt='Thomas' />
                    ) : null}
                  </span>
                  {rule.hasImage ? '.' : null}
                </p>
              </div>
            );
          })}
        </div>

        <div className={styles.buttonBlock}>
          <a href={leaflet} download>
            <button>Скачать буклет</button>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Rules;
