import React from 'react';
import styles from './Header.module.css';

import thomasLogo from '../../assets/images/thomas-black.svg';
import victoriaLogo from '../../assets/images/victoria.png';
import salesCircle from '../../assets/images/sales-circle.png';
import card from '../../assets/images/card.png';
import littleCard from '../../assets/images/little-card.png';

const WIDTH = window.innerWidth;

const Header = () => {
  return (
    <header className={styles.header}>
      <div className='container'>
        <div className={styles.wrap}>
          <div className={styles.row}>
            <div className={styles.thomas}>
              <img src={thomasLogo} alt='Thomas logo' />
            </div>
            <div className={styles.victoria}>
              <img src={victoriaLogo} alt='Victoria logo' />
            </div>
            <p className={styles.subtitle}>с 19 июля по 10 октября</p>
          </div>
          <div className={styles.signature}>
            <p>ЭКСКЛЮЗИВНЫЕ СТОЛОВЫЕ ПРИБОРЫ</p>
          </div>

          <div className={styles.sale}>
            <div className={styles.saleCircle}>
              <img src={salesCircle} alt='Sale' />
            </div>
            <p className={styles.salesParagraph}>
              Собирайте наклейки, получайте скидки
            </p>

            <div className={styles.cards}>
              <img src={card} alt='Card' />
              <img src={card} alt='Card' />
            </div>
          </div>

          <h1 className={styles.title}>СОБИРАЙТЕ СЕМЬЮ ЗА СТОЛОМ!</h1>
          <div className={styles.equality}>
            <div className={styles.bullet}>
              <span className={styles.redBg}>300&nbsp;₽</span>&nbsp;=&nbsp;
              <span className={styles.one}>&nbsp;1&nbsp;</span>
            </div>
            <img src={littleCard} alt='Card' />
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
