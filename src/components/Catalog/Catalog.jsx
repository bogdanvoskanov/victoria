import React from 'react';
import styles from './Catalog.module.css';

import classnames from 'classnames';

import thomasLogo from '../../assets/images/thomas-black.svg';
import benefits from '../../assets/images/benefits.png';
import littleCard from '../../assets/images/little-card.png';

import cutlery1 from '../../assets/images/cutlery-1.png';
import cutlery2 from '../../assets/images/cutlery-2.png';
import cutlery3 from '../../assets/images/cutlery-3.png';

import knife1 from '../../assets/images/knife1.png';
import knife2 from '../../assets/images/knife2.png';

const Catalog = () => {
  const [products, setProducts] = React.useState([
    {
      img: cutlery1,
      name: 'Набор чайных ложек',
      count: 4,
      price: 299,
      retailPrice: 1399,
      isKnife: false
    },
    {
      img: cutlery2,
      name: 'Набор десертных вилок',
      count: 4,
      price: 349,
      retailPrice: 1399,
      isKnife: false
    },
    {
      img: cutlery3,
      name: 'Детский набор классика',
      count: 3,
      price: 349,
      retailPrice: 1399,
      isKnife: false
    },
    {
      img: knife1,
      name: 'Нож универсальный',
      price: 399,
      retailPrice: 2999,
      length: 11.5,
      isKnife: true,
      isFirstKnife: true
    },
    {
      img: knife2,
      name: 'Нож поварской',
      price: 499,
      retailPrice: 3599,
      length: 19,
      isKnife: true
    }
  ]);

  return (
    <div className={styles.catalog}>
      <div className={styles.logo}>
        <img src={thomasLogo} alt='Thomas' />
      </div>
      <div className={styles.description}>
        <p className={styles.definition}>
          <strong>THOMAS</strong> – ведущая торговая марка из Германии со
          100-летней историей, производящая стильные и удобные товары для кухни
          и сервировки стола.
        </p>
        <p className={styles.quality}>
          Произведенные из высококачественной стали, приборы Thomas не впитывают
          запахи, устойчивы к воздействию кислот, не подвержены коррозии, не
          гнутся и не ломаются. Зеркально отполированная поверхность не теряет
          свой блеск и гладкость с течением времени.
        </p>
      </div>
      <div className={styles.items}>
        <div className={styles.grid}>
          {products.map((p, index) => {
            return (
              <div
                key={index}
                className={classnames(styles.item, {
                  [styles.knife]: p.isKnife,
                  [styles.firstKnife]: p.isFirstKnife
                })}>
                <div className={styles.image}>
                  <img src={p.img} alt='' />
                </div>
                <div className={styles.info}>
                  <h4 className={styles.title}>{p.name}</h4>
                  {!p.isKnife ? (
                    <p className={styles.count}>{p.count} предмета</p>
                  ) : (
                    <p className={styles.count}>{p.length} см</p>
                  )}

                  <div className={styles.price}>
                    <span>{p.price}&nbsp;руб.</span>&nbsp;+&nbsp;15
                    <img src={littleCard} alt='Card' />
                  </div>
                  <div className={styles.retail}>
                    Розничная цена - {p.retailPrice}
                  </div>
                </div>
              </div>
            );
          })}
          <div>
            <div className={styles.thomas}>
              <img src={thomasLogo} alt='' />
            </div>
            <div className={styles.benefits}>
              <img src={benefits} alt='' />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Catalog;
